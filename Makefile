.ONESHELL:

all: reinit

reinit: FORCE
	rm -rf .pio
	find . -type d -name "cmake-build-*" -exec rm -rf {} +
	platformio -c clion init --ide clion

#
# Programs
#

.PHONY: FORCE
FORCE:
